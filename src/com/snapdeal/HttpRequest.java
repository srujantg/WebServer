package com.snapdeal;

import java.awt.List;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.NavigableMap;
import java.util.TreeMap;

public class HttpRequest {
	
	private String protocol;
	private String url;
	private String method;
	private NavigableMap<String, String> headers = new TreeMap<String, String>();
	private ArrayList<String> body = new ArrayList<String>();


	public HttpRequest(){
		
	}
	
	public String getUrl() {
		return this.url;
	}

	public void parseHttp(InputStream in) throws Exception {
		// TODO Auto-generated method stub
			BufferedReader reader = new BufferedReader(new InputStreamReader(in));
			String line = reader.readLine();
			System.out.println(line);
			String [] tokens = line.split(" ", 3);
			
			if(tokens.length != 3)
				throw new IOException("Request format error in : " + line);
			
			if(!tokens[2].startsWith("HTTP"))
				throw new IOException("Only HTTP requests are served");
			
			this.method = tokens[0];
			this.url = tokens[1];
			this.protocol = tokens[2];
			
			line = reader.readLine();
			System.out.println(line);
			while ( line != null && !line.equals("") ){
				String [] heads = line.split(": ", 2);
				if (heads.length != 2)
					throw new IOException("Request format error in : "+ line);
				this.headers.put(heads[0], heads[1]);
				line = reader.readLine();
				System.out.println(line);
			}
			
			while (reader.ready()){
				this.body.add(reader.readLine());
			}
	}
}
