package com.snapdeal;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server implements Runnable {
	
	private int port;
	private String rootDir;
	private ServerSocket server;
	private ExecutorService threadsPool;
	private int maxThreads;

	public Server(int port, String rootDir, int maxThreads) {
		// TODO Auto-generated constructor stub
		this.port = port;
		this.rootDir = rootDir;
		this.maxThreads = maxThreads;
	}

	public static void main(String args[]){
		int port = 6789;
		String rootDirectory = "rootDir";
		int maxThreads = 10;
		Thread t = new Thread(new Server(port, rootDirectory, maxThreads));
		t.start();
	}
	
	public String getRoot() {
		return this.rootDir;
	}
	
	@Override
	public void run(){
		
		try {
			server = new ServerSocket(port);
			threadsPool = Executors.newFixedThreadPool(maxThreads);
		} catch (IOException e) {
			System.err.println("Cannot listen on port " + port);
			System.exit(1);
		}
		
		System.out.println("Running server on the port " + port + 
				" with web root folder \"" + rootDir + "\" and " + maxThreads + " threads limit.");

		while (!Thread.interrupted()) {
			try {
				threadsPool.execute(new Thread(new Connection(server.accept(), this)));
			} catch (IOException e) {
				System.err.println("Cannot accept client.");
			}
		}
	}
}
