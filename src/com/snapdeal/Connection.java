package com.snapdeal;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;

import com.snapdeal.HttpResponse.StatusCode;

public class Connection implements Runnable {

	private Server server;
	private Socket socket;
	private InputStream in;
	private OutputStream out;
	

	public Connection(Socket accept, Server server) {
		// TODO Auto-generated constructor stub
		this.socket = accept;
		this.server = server;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		
		try {
			in = socket.getInputStream();
			out = socket.getOutputStream();
			
			HttpRequest request = new HttpRequest();		
			try{
				request.parseHttp(in);
			
				HttpResponse response = new HttpResponse(StatusCode.OK);
				
				response = response.withFile(server.getRoot() + request.getUrl());
				sendResponse(response);
			}
			catch (Exception e) {
				System.out.println(e.getMessage());
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				socket.close();
			} catch (IOException e) {
				System.out.println("Error while closing client socket." + e.getMessage());
			}
		}
		
	}
	
	public void sendResponse(HttpResponse response) {
		String toSend = response.toString();
		PrintWriter writer = new PrintWriter(out);
		writer.write(toSend);
		writer.flush();
	}

}
